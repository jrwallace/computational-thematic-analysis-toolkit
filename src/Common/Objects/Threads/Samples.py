import logging
from threading import Thread
import os
import psutil
import bz2
import pickle
import numpy
import pickle
import torch
import numpy as np
import torch.multiprocessing as mp
import pandas as pd
import os
import time
import random
import json
import wx

# ML libraries
import gensim
# If not getting updated, use this code https://github.com/maximtrp/bitermplus
import bitermplus as btm
from octis.dataset.dataset import Dataset

from sklearn.decomposition import NMF as nmf
from sklearn.feature_extraction.text import TfidfVectorizer
from bertopic import BERTopic
from sentence_transformers import SentenceTransformer
from umap import UMAP
from hdbscan import HDBSCAN
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer
from bertopic.vectorizers import ClassTfidfTransformer
from gensim.models.coherencemodel import CoherenceModel
from sklearn.pipeline import make_pipeline
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sentence_transformers import SentenceTransformer, util
from sklearn.metrics.pairwise import cosine_similarity
from gensim.models import CoherenceModel
from gensim.corpora import Dictionary
from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
from gensim.models import CoherenceModel
from gensim import corpora
from scipy.spatial.distance import jensenshannon
from sklearn.model_selection import train_test_split

from octis.models.ETM import ETM
from octis.models.ETM_model import etm
from octis.evaluation_metrics.diversity_metrics import TopicDiversity
from octis.evaluation_metrics.coherence_metrics import Coherence
from scipy.special import rel_entr

# from octis.models.LDA import LDA
# from octis.dataset.dataset import Dataset
# from octis.evaluation_metrics.diversity_metrics import TopicDiversity
# from octis.evaluation_metrics.coherence_metrics import Coherence


import Common.CustomEvents as CustomEvents
import Common.Objects.Utilities.Samples as SamplesUtilities

# New Code for Bertopic
from abc import ABC, abstractmethod


# New Code for Bertopic
class AbstractMetric(ABC):
    """
    Class structure of a generic metric implementation
    """

    def __init__(self):
        """
        init metric
        """
        pass

    @abstractmethod
    def score(self, model_output):
        """
        Retrieves the score of the metric

        :param model_output: output of a topic model in the form of a dictionary. See model for details on
        the model output
        :type model_output: dict
        """
        pass

    def get_params(self):
        return [att for att in dir(self) if not att.startswith("_") and att != 'info' and att != 'score' and
                att != 'get_params']


class MyETM(ETM):
    def set_model(self, dataset, hyperparameters):
        if self.use_partitions:
            train_data, testing_data = (
                dataset.get_partitioned_corpus(use_validation=False))

            data_corpus_train = [' '.join(i) for i in train_data]
            data_corpus_test = [' '.join(i) for i in testing_data]

            vocab = dataset.get_vocabulary()
            self.vocab = {i: w for i, w in enumerate(vocab)}
            vocab2id = {w: i for i, w in enumerate(vocab)}

            (self.train_tokens, self.train_counts, self.test_tokens,
             self.test_counts
             ) = self.preprocess(
                vocab2id, data_corpus_train, data_corpus_test)

        else:
            data_corpus = [' '.join(i) for i in dataset.get_corpus()]
            vocab = dataset.get_vocabulary()
            self.vocab = {i: w for i, w in enumerate(vocab)}
            vocab2id = {w: i for i, w in enumerate(vocab)}

            self.train_tokens, self.train_counts = self.preprocess(
                vocab2id, data_corpus, None)

        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu")

        self.set_default_hyperparameters(hyperparameters)
        self.load_embeddings()
        # define model and optimizer
        self.model = etm.ETM(
            num_topics=self.hyperparameters['num_topics'],
            vocab_size=len(self.vocab.keys()),
            t_hidden_size=int(self.hyperparameters['t_hidden_size']),
            rho_size=int(self.hyperparameters['rho_size']),
            emb_size=int(self.hyperparameters['embedding_size']),
            theta_act=self.hyperparameters['activation'],
            embeddings=self.embeddings,
            train_embeddings=self.hyperparameters['train_embeddings'],
            enc_drop=self.hyperparameters['dropout']).to(self.device)
        print('model: {}'.format(self.model))

        self.optimizer = self.set_optimizer()


# New Code for Bertopic
class TopicDiversity(AbstractMetric):
    def __init__(self, topk=20):
        """
        Initialize metric

        Parameters
        ----------
        topk: top k words on which the topic diversity will be computed
        """
        AbstractMetric.__init__(self)
        self.topk = topk

    def info(self):
        return {
            "citation": citations.em_topic_diversity,
            "name": "Topic diversity"
        }

    def score(self, model_output):
        """
        Retrieves the score of the metric

        Parameters
        ----------
        model_output : dictionary, output of the model
                       key 'topics' required.

        Returns
        -------
        td : score
        """
        topics = model_output.get("topics")
        if topics is None:
            return 0
        if not topics:  # Check if topics is an empty list
            return 0
        if self.topk > len(topics[0]):
            raise Exception('Words in topics are less than ' + str(self.topk))
        else:
            unique_words = set()
            for topic in topics:
                unique_words = unique_words.union(set(topic[:self.topk]))
            td = len(unique_words) / (self.topk * len(topics))
            return td


class CaptureThread(Thread):
    def __init__(self, notify_window, main_frame, model_paramaters, model_type):
        Thread.__init__(self)
        self._notify_window = notify_window
        self.main_frame = main_frame
        self.model_parameters = model_paramaters
        self.model_type = model_type
        self.start()

    def run(self):
        dataset_key = self.model_parameters['dataset_key']
        self.model_parameters['tokensets'], field_list = SamplesUtilities.CaptureTokens(dataset_key, self.main_frame)
        wx.PostEvent(self._notify_window,
                     CustomEvents.CaptureResultEvent(self.model_type, self.model_parameters, field_list))


class LDATrainingThread(Thread):
    """LDATrainingThread Class."""

    def __init__(self, notify_window, current_workspace_path, key, tokensets, num_topics, num_passes, alpha, eta):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.daemon = True
        self._notify_window = notify_window
        self.current_workspace_path = current_workspace_path
        self.key = key
        self.tokensets = tokensets
        self.num_topics = num_topics
        self.num_passes = num_passes
        self.alpha = alpha
        self.eta = eta
        self.start()

    def run(self):
        '''Generates an LDA model'''
        logger = logging.getLogger(__name__ + "LDATrainingThread[" + str(self.key) + "].run")
        logger.info("Starting")
        t1 = time.time()
        if not os.path.exists(self.current_workspace_path + "/Samples/" + self.key):
            os.makedirs(self.current_workspace_path + "/Samples/" + self.key)

        logger.info("Starting generation of model")
        tokensets_keys = list(self.tokensets.keys())
        tokensets_values = list(self.tokensets.values())
        dictionary = gensim.corpora.Dictionary(tokensets_values)
        dictionary.compactify()
        dictionary.save(self.current_workspace_path + "/Samples/" + self.key + '/ldadictionary.dict')
        logger.info("Dictionary created")
        raw_corpus = [dictionary.doc2bow(tokenset) for tokenset in tokensets_values]
        gensim.corpora.MmCorpus.serialize(self.current_workspace_path + "/Samples/" + self.key + '/ldacorpus.mm',
                                          raw_corpus)
        corpus = gensim.corpora.MmCorpus(self.current_workspace_path + "/Samples/" + self.key + '/ldacorpus.mm')

        logger.info("Corpus created")

        if self.alpha is not None:
            alpha = self.alpha
        else:
            alpha = 'symmetric'
        if self.eta is not None:
            eta = self.eta
        else:
            eta = 'auto'

        cpus = psutil.cpu_count(logical=False)
        if cpus is None or cpus < 2:
            workers = 1
        else:
            workers = cpus - 1

        model = gensim.models.ldamulticore.LdaMulticore(workers=workers,
                                                        corpus=corpus,
                                                        id2word=dictionary,
                                                        num_topics=self.num_topics,
                                                        passes=self.num_passes,
                                                        alpha=alpha,
                                                        eta=eta)
        model.save(self.current_workspace_path + "/Samples/" + self.key + '/ldamodel.lda', 'wb')
        logger.info("Completed generation of model")

        start_time = time.time()
        lda_coherence_model = gensim.models.CoherenceModel(model=model, texts=tokensets_values, dictionary=dictionary,
                                                           corpus=corpus, coherence='c_v')
        lda_coherence_value = lda_coherence_model.get_coherence()
        print("================================================")
        print("Topic Coherence for LDA: ", lda_coherence_value)

        # Calculate topic diversity
        topk = 10  # Number of top words to consider for each topic
        all_top_words = model.show_topics(num_topics=self.num_topics, num_words=topk, formatted=False)

        unique_words = set()
        for topic_id, topic_words in all_top_words:
            # Add the topk words of each topic to the unique words set
            words_in_topic = [word for word, _ in topic_words]
            unique_words.update(words_in_topic)

        # Calculate topic diversity
        total_unique_words = len(unique_words)
        topic_diversity = total_unique_words / (self.num_topics * topk)

        # Print and log the topic diversity
        print(f"Topic Diversity for LDA: {topic_diversity:.4f}")
        logger.info(f"Topic Diversity for LDA: {topic_diversity:.4f}")

        # Calculate KL divergence (average across all topics)
        kl_divergence = 0
        count = 0

        for i in range(self.num_topics):
            for j in range(i + 1, self.num_topics):
                # Get topic-word distributions for topics i and j
                p = np.array(model.get_topic_terms(i))
                q = np.array(model.get_topic_terms(j))

                # Normalize the distributions
                p = p[:, 1] / p[:, 1].sum()
                q = q[:, 1] / q[:, 1].sum()

                # Calculate KL divergence for the topic pair
                kl_div = np.sum(p * np.log(p / q + 1e-9))  # Add a small epsilon to avoid log(0)
                # Check for invalid KL divergence and skip if necessary
                if np.isnan(kl_div) or np.isinf(kl_div):
                    continue

                kl_divergence += kl_div
                count += 1

        # Calculate average KL divergence across all topic pairs
        if count > 0:
            avg_kl_divergence = kl_divergence / count
        else:
            avg_kl_divergence = 0

        # Print and log the average KL divergence
        print(f"Average KL Divergence for LDA: {avg_kl_divergence:.4f}")
        logger.info(f"Average KL Divergence for LDA: {avg_kl_divergence:.4f}")

        # Calculate perplexity
        perplexity_1 = model.log_perplexity(corpus)  # Perplexity calculation
        perplexity = np.exp2(perplexity_1)

        # Print and log the perplexity
        print(f"Perplexity for LDA: {perplexity:.4f}")
        logger.info(f"Perplexity for LDA: {perplexity:.4f}")

        # End measuring the time taken from coherence to perplexity
        end_time = time.time()
        time_taken = end_time - start_time

        # Print and log the time taken
        print(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        t2 = time.time()
        tots = t2 - t1
        print(f"Time taken Total: {tots:.4f} seconds")
        print("================================================")
        logger.info(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        # Init output
        # capture all document topic probabilities both by document and by topic
        document_topic_prob = {}
        model_document_topics = model.get_document_topics(corpus, minimum_probability=0.0, minimum_phi_value=0)
        print(model_document_topics)
        for doc_num in range(len(corpus)):
            doc_row = model_document_topics[doc_num]
            doc_topic_prob_row = {}
            for i, prob in doc_row:
                doc_topic_prob_row[i + 1] = prob
            document_topic_prob[tokensets_keys[doc_num]] = doc_topic_prob_row

        logger.info("Finished")
        result = {'key': self.key, 'document_topic_prob': document_topic_prob}
        print(result)
        wx.PostEvent(self._notify_window, CustomEvents.ModelCreatedResultEvent(result))


class BitermTrainingThread(Thread):
    """BitermTrainingThread Class."""

    def __init__(self, notify_window, current_workspace_path, key, tokensets, num_topics, num_passes):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.daemon = True
        self._notify_window = notify_window
        self.current_workspace_path = current_workspace_path
        self.key = key
        self.tokensets = tokensets
        self.num_topics = num_topics
        self.num_passes = num_passes
        self.start()

    def run(self):
        '''Generates an Biterm model'''
        logger = logging.getLogger(__name__ + "BitermTrainingThread[" + str(self.key) + "].run")
        logger.info("Starting")

        if not os.path.exists(self.current_workspace_path + "/Samples/" + self.key):
            os.makedirs(self.current_workspace_path + "/Samples/" + self.key)

        text_keys = []
        texts = []
        for key in self.tokensets:
            text_keys.append(key)
            text = ' '.join(self.tokensets[key])
            texts.append(text)

        logger.info("Starting generation of biterm model")

        X, vocab, vocab_dict = btm.get_words_freqs(texts)
        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/vocab.pk', 'wb') as outfile:
            pickle.dump(vocab, outfile)
        logger.info("Vocab created")

        # Vectorizing documents
        docs_vec = btm.get_vectorized_docs(texts, vocab)
        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/transformed_texts.pk',
                         'wb') as outfile:
            pickle.dump(docs_vec, outfile)
        logger.info("Texts transformed")

        logger.info("Starting Generation of BTM")
        biterms = btm.get_biterms(docs_vec)

        model = btm.BTM(X, vocab, T=self.num_topics, M=20, alpha=50 / 8, beta=0.01)
        p_zd = model.fit_transform(docs_vec, biterms, iterations=self.num_passes, verbose=False)
        print(p_zd)
        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/btm.pk', 'wb') as outfile:
            pickle.dump(model, outfile)
        logger.info("Completed Generation of BTM")

        document_topic_prob = {}
        for doc_num in range(len(p_zd)):
            doc_row = p_zd[doc_num]
            doc_topic_prob_row = {}
            for i in range(len(doc_row)):
                doc_topic_prob_row[i + 1] = doc_row[i]
            document_topic_prob[text_keys[doc_num]] = doc_topic_prob_row

        logger.info("Finished")
        result = {'key': self.key, 'document_topic_prob': document_topic_prob}
        print(result)
        wx.PostEvent(self._notify_window, CustomEvents.ModelCreatedResultEvent(result))


class NMFTrainingThread(Thread):
    """NMFTrainingThread Class."""

    def __init__(self, notify_window, current_workspace_path, key, tokensets, num_topics):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.daemon = True
        self._notify_window = notify_window
        self.current_workspace_path = current_workspace_path
        self.key = key
        self.tokensets = tokensets
        self.num_topics = num_topics
        self.start()

    def run(self):
        '''Generates an NMF model'''
        logger = logging.getLogger(__name__ + "NMFTrainingThread[" + str(self.key) + "].run")
        logger.info("Starting")
        t1 = time.time()

        if not os.path.exists(self.current_workspace_path + "/Samples/" + self.key):
            os.makedirs(self.current_workspace_path + "/Samples/" + self.key)

        text_keys = []
        texts = []
        for key in self.tokensets:
            text_keys.append(key)
            text = ' '.join(self.tokensets[key])
            texts.append(text)

        logger.info("Starting generation of NMF model")

        tfidf_vectorizer = TfidfVectorizer(max_features=len(self.tokensets.values()),
                                           preprocessor=SamplesUtilities.dummy, tokenizer=SamplesUtilities.dummy,
                                           token_pattern=None)

        tfidf = tfidf_vectorizer.fit_transform(self.tokensets.values())
        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/tfidf.pk', 'wb') as outfile:
            pickle.dump(tfidf, outfile)

        logger.info("Texts transformed")

        logger.info("Starting Generation of NMF")

        model = nmf(self.num_topics, random_state=1).fit(tfidf)

        # must fit tfidf as above before saving tfidf_vectorizer
        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/tfidf_vectorizer.pk',
                         'wb') as outfile:
            pickle.dump(tfidf_vectorizer, outfile)

        topics = tfidf_vectorizer.get_feature_names_out()
        topic_pr = model.transform(tfidf)
        topic_pr_sum = numpy.sum(topic_pr, axis=1, keepdims=True)
        probs = numpy.divide(topic_pr, topic_pr_sum, out=numpy.zeros_like(topic_pr), where=topic_pr_sum != 0)

        terms = tfidf_vectorizer.get_feature_names_out()

        # Create a dictionary mapping term indices to terms
        term_dict = {idx: term for idx, term in enumerate(terms)}

        # Extract topics from the NMF model
        topic_words = []
        for topic_idx in range(self.num_topics):
            # Get the top words for each topic
            top_word_indices = np.argsort(model.components_[topic_idx])[::-1][:10]
            topic_words.append([term_dict[idx] for idx in top_word_indices])

        # Create a Gensim Dictionary from the texts
        dictionary = Dictionary([text.split() for text in texts])
        start_time = time.time()
        # Calculate topic coherence using the 'c_v' metric
        coherence_model = CoherenceModel(topics=topic_words, texts=[text.split() for text in texts],
                                         dictionary=dictionary, coherence='c_v')
        coherence_score = coherence_model.get_coherence()
        print("================================================")
        print("Coherence of NMF:", coherence_score)

        # Calculate topic diversity
        topk = 10  # Number of top words to consider for each topic
        words = tfidf_vectorizer.get_feature_names_out()
        topic_word_matrix = model.components_

        unique_words = set()
        for topic_id in range(self.num_topics):
            # Get the indices of the top k words in the topic
            topk_indices = np.argsort(topic_word_matrix[topic_id])[-topk:]
            # Add the top k words in this topic to the set of unique words
            unique_words.update(words[i] for i in topk_indices)

        # Calculate topic diversity
        total_unique_words = len(unique_words)
        topic_diversity = total_unique_words / (topk * self.num_topics)

        # Print and log the topic diversity
        print(f"Topic Diversity for NMF: {topic_diversity:.4f}")
        logger.info(f"Topic Diversity for NMF: {topic_diversity:.4f}")

        # Calculate KL divergence (average across all topics)
        topic_distributions = model.components_
        kl_divergence = 0
        count = 0
        epsilon = 1e-9  # A small constant to avoid log(0) and division by zero

        for i in range(self.num_topics):
            for j in range(i + 1, self.num_topics):
                # Normalize the distributions
                p = topic_distributions[i] / topic_distributions[i].sum()
                q = topic_distributions[j] / topic_distributions[j].sum()

                # Check for zero values and adjust with epsilon
                p = np.maximum(p, epsilon)
                q = np.maximum(q, epsilon)

                # Calculate KL divergence for the topic pair
                kl_div = np.sum(p * np.log(p / q))

                # Check for invalid KL divergence and skip if necessary
                if np.isnan(kl_div) or np.isinf(kl_div):
                    continue

                kl_divergence += kl_div
                count += 1

        # Calculate average KL divergence across all topic pairs
        if count > 0:
            avg_kl_divergence = kl_divergence / count
        else:
            avg_kl_divergence = 0

        # Print and log the average KL divergence
        print(f"Average KL Divergence for NMF: {avg_kl_divergence:.4f}")
        logger.info(f"Average KL Divergence for NMF: {avg_kl_divergence:.4f}")

        # Calculate perplexity
        document_topic_matrix = model.transform(tfidf)
        perplexity = 0
        doc_count = tfidf.shape[0]
        word_count = tfidf.sum(axis=1)

        for doc_idx in range(doc_count):
            doc_words = tfidf[doc_idx].toarray().flatten()
            doc_topic_distribution = document_topic_matrix[doc_idx]
            predicted_word_distribution = model.inverse_transform([doc_topic_distribution])[0]

            # Calculate the log likelihood of the document given the model
            log_likelihood = np.sum(doc_words * np.log(predicted_word_distribution + 1e-9))
            perplexity -= log_likelihood

        # Calculate average perplexity
        if doc_count > 0:
            perplexity = np.exp(perplexity / np.sum(word_count))

        # Print and log the perplexity
        print(f"Perplexity for NMF: {perplexity:.4f}")

        logger.info(f"Perplexity for NMF: {perplexity:.4f}")
        # End measuring the time taken from coherence to perplexity
        end_time = time.time()
        time_taken = end_time - start_time

        # Print and log the time taken
        print(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        t2 = time.time()
        tots = t2 - t1
        print(f"Time taken Total: {tots:.4f} seconds")
        print("================================================")
        logger.info(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")

        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/nmf_model.pk', 'wb') as outfile:
            pickle.dump(model, outfile)
        logger.info("Completed Generation of NMF")

        document_topic_prob = {}
        for doc_num in range(len(probs)):
            doc_row = probs[doc_num]
            doc_topic_prob_row = {}
            for i in range(len(doc_row)):
                doc_topic_prob_row[i + 1] = doc_row[i]
            document_topic_prob[text_keys[doc_num]] = doc_topic_prob_row

        logger.info("Finished")
        result = {'key': self.key, 'document_topic_prob': document_topic_prob}
        wx.PostEvent(self._notify_window, CustomEvents.ModelCreatedResultEvent(result))


# New Code for Bertopic
class BertopicTrainingThread(Thread):
    """BERTopicTrainingThread Class."""

    def __init__(self, notify_window, current_workspace_path, key, tokensets, num_topics):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.daemon = True
        self._notify_window = notify_window
        self.current_workspace_path = current_workspace_path
        self.key = key
        self.tokensets = tokensets
        self.num_topics = num_topics
        self.start()
        os.environ["TOKENIZERS_PARALLELISM"] = "false"

    def run(self):
        '''Generates a BERTopic model'''
        logger = logging.getLogger(__name__ + "BertopicTrainingThread[" + str(self.key) + "].run")
        logger.info("Starting")
        t1 = time.time()

        if not os.path.exists(self.current_workspace_path + "/Samples/" + self.key):
            os.makedirs(self.current_workspace_path + "/Samples/" + self.key)

        text_keys = []
        texts = []
        for key in self.tokensets:
            text_keys.append(key)
            text = ' '.join(self.tokensets[key])
            texts.append(text)

        logger.info("Starting generation of BERTopic model")

        model = SentenceTransformer("all-MiniLM-L6-v2")
        embedding_model = model.encode(texts, show_progress_bar=False)
        umap_model = UMAP(n_neighbors=15, n_components=5, min_dist=0.0, metric='cosine')
        hdbscan_model = HDBSCAN(min_cluster_size=15, metric='euclidean', cluster_selection_method='eom',
                                prediction_data=True)
        vectorizer_model = CountVectorizer(stop_words="english")
        ctfidf_model = ClassTfidfTransformer()

        # Initialize BERTopic model
        topic_model = BERTopic(
            embedding_model=model,  # Step 1 - Extract embeddings
            umap_model=umap_model,  # Step 2 - Reduce dimensionality
            hdbscan_model=hdbscan_model,  # Step 3 - Cluster reduced embeddings
            vectorizer_model=vectorizer_model,  # Step 4 - Tokenize topics
            ctfidf_model=ctfidf_model,  # Step 5 - Extract topic words
            nr_topics="auto",  # Step 6 - Diversify topic words
            calculate_probabilities=True
        )

        print("Pipeline created successfully")
        print("BERTopic model initialized successfully")

        # Fit BERTopic model on the text data
        topics, document_topic_prob = topic_model.fit_transform(texts)
        # print("-----------------")
        # print(document_topic_prob)

        updated_topic_info = topic_model.get_topics()
        print("Updated topic information:")
        for topic_id, topic_words in updated_topic_info.items():
            print(f"Topic {topic_id}:")
            for word, prob in topic_words:
                print(f"- {word} (probability: {prob:.4f})")

        # self.num_topics = topic_model.nr_topics
        # print( "before", self.num_topics )
        print("topic_number :", document_topic_prob.shape[1])
        # self.num_topics = document_topic_prob.shape[1] 
        # print("new topics is", self.num_topics)

        documents = pd.DataFrame({"Document": texts,
                                  "ID": range(len(texts)),
                                  "Topic": topics})
        documents_per_topic = documents.groupby(['Topic'], as_index=False).agg({'Document': ' '.join})
        cleaned_docs = topic_model._preprocess_text(documents_per_topic.Document.values)

        # Extract vectorizer and analyzer from BERTopic
        vectorizer = topic_model.vectorizer_model
        analyzer = vectorizer.build_analyzer()

        # Extract features for Topic Coherence evaluation
        words = vectorizer.get_feature_names_out()
        tokens = [analyzer(doc) for doc in cleaned_docs]
        dictionary = corpora.Dictionary(tokens)
        corpus = [dictionary.doc2bow(token) for token in tokens]
        topic_words = [[words for words, _ in topic_model.get_topic(topic)]
                       for topic in range(len(set(topics)) - 1)]

        # Evaluate
        coherence_model = CoherenceModel(topics=topic_words,
                                         texts=tokens,
                                         corpus=corpus,
                                         dictionary=dictionary,
                                         coherence='c_v')
        coherence = coherence_model.get_coherence()
        start_time = time.time()
        print("================================================")
        # Print coherence score
        print(f'Topic Coherence for Bertopic: {coherence}')

        file_path = "model_topics_list.txt"

        with open(file_path, "w") as file:
            # Instantiate the TopicDiversity metric
            metric = TopicDiversity(topk=10)
            model_output = {"topics": []}

            topic = 0
            while True:
                # Get the topic information using topic_model.get_topic(topic)
                topic_info = topic_model.get_topic(topic)
                print(topic_info)

                # If the topic information is empty, break out of the loop
                if not topic_info:
                    break

                # Append the topic information to the list under the "topics" key
                model_output["topics"].append(topic_info)

                # Write the topic number
                file.write(f"Topic {topic}:\n")

                # Write the keywords and their probabilities
                for keyword, probability in topic_info:
                    file.write(f"('{keyword}', {probability})\n")

                # Write a newline to separate topics
                file.write("\n")

                # Increment the topic index
                topic += 1

        topic_diversity_score = metric.score(model_output)
        print("Topic Diversity Score is: {0}".format(topic_diversity_score))

        # Calculate KL divergence (average across all topics)
        kl_divergence = 0
        count = 0
        epsilon = 1e-9  # Small constant to avoid log(0) and division by zero

        topic_distributions = updated_topic_info

        for i in range(len(topic_distributions)):
            for j in range(i + 1, len(topic_distributions)):
                # Get topic-word distributions for topics i and j
                topic_i = topic_model.get_topic(i)
                topic_j = topic_model.get_topic(j)

                # Check if topic_i and topic_j are valid lists or dictionaries
                if not isinstance(topic_i, (list, dict)) or not isinstance(topic_j, (list, dict)):
                    continue  # Skip this iteration if the output is not a valid list or dictionary

                # Convert topic distributions to dictionaries if they are lists
                dist_i = dict(topic_i) if isinstance(topic_i, list) else topic_i
                dist_j = dict(topic_j) if isinstance(topic_j, list) else topic_j

                # Find the intersection of words between the two topics
                common_words = set(dist_i.keys()) & set(dist_j.keys())

                # Calculate KL divergence for the common words between the topic pair
                kl_div = 0
                for word in common_words:
                    p = dist_i[word]
                    q = dist_j[word]
                    if p > 0 and q > 0:
                        kl_div += p * np.log(p / q)

                # Check for invalid KL divergence and skip if necessary
                if np.isnan(kl_div) or np.isinf(kl_div):
                    continue

                kl_divergence += kl_div
                count += 1

            # Calculate average KL divergence across all topic pairs
            if count > 0:
                avg_kl_divergence = kl_divergence / count
            else:
                avg_kl_divergence = 0

            # Print and log the average KL divergence
        print(f"Average KL Divergence for BERTopic: {avg_kl_divergence:.4f}")
        logger.info(f"Average KL Divergence for BERTopic: {avg_kl_divergence:.4f}")

        log_perplexity = -1 * np.mean(np.log(np.sum(document_topic_prob, axis=0)))
        perplexity = np.exp(log_perplexity)

        # Print and log the perplexity
        print(f"Perplexity for BERTopic: {perplexity:.4f}")
        logger.info(f"Perplexity for BERTopic: {perplexity:.4f}")

        # End measuring the time taken from coherence to perplexity
        end_time = time.time()
        time_taken = end_time - start_time

        # Print and log the time taken
        print(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        logger.info(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        t2 = time.time()
        tots = t2 - t1
        print(f"Time taken Total: {tots:.4f} seconds")
        print("================================================")

        logger.info("Completed BERTopic model and calculated KL divergence and perplexity")
        logger.info("Finished")

        # doc_info = topic_model.get_document_info(texts)
        # doc_df = pd.DataFrame(doc_info)
        # doc_info_csv_file = "doc_info_bertopic.csv"
        # doc_df.to_csv(doc_info_csv_file, index=False)

        logger.info("Texts transformed")

        logger.info("Starting Generation of BERTopic")

        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/tfidf_vectorizer.pk',
                         'wb') as outfile:
            pickle.dump(vectorizer_model, outfile)

        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/tfidf.pk', 'wb') as outfile:
            pickle.dump(ctfidf_model, outfile)

        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/bertopic_model.pk', 'wb') as outfile:
            pickle.dump(topic_model, outfile)

        logger.info("Completed Generation of BERTopic")
        # Fit the BERTopic model on the text data

        # Filter columns to include only topics with positive indices
        positive_topic_indices = [i for i in range(document_topic_prob.shape[1]) if i > 0]
        filtered_topic_prob_df = pd.DataFrame(document_topic_prob[:, positive_topic_indices],
                                              columns=[f"Topic {i}" for i in positive_topic_indices])

        # Print the filtered DataFrame
        # print(filtered_topic_prob_df)

        # Prepare the result dictionary
        result = {'key': self.key, 'document_topic_prob': {}}
        print("----------Text keys----------")
        print(text_keys)
        print("-----------------------------")
        print("-------Filtered topic values-------")
        print(filtered_topic_prob_df.values)
        print("-----------------------------------")

        for i, row in enumerate(filtered_topic_prob_df.values):
            formatted_key = text_keys[i]  # Use the document key (text key)
            result['document_topic_prob'][formatted_key] = {j + 1: prob for j, prob in enumerate(row)}

        # # Convert the NumPy array to a DataFrame
        # document_topic_prob_df = pd.DataFrame(document_topic_prob, columns=[f"Topic {i}" for i in range(document_topic_prob.shape[1])])
        # print(document_topic_prob_df)

        # result = {'key': self.key, 'document_topic_prob': {}}

        # for i, row in enumerate(document_topic_prob_df.values):
        #     formatted_key = text_keys[i]  # Remove the extra ()
        #     result['document_topic_prob'][formatted_key] = {j + 1: prob for j, prob in enumerate(row)}

        # print(result)
        print("---------Result-------------")
        print(result)
        print("----------------------------")
        wx.PostEvent(self._notify_window, CustomEvents.ModelCreatedResultEvent(result))


class ETMTrainingThread(Thread):
    """ETMTrainingThread Class."""

    def __init__(self, notify_window, current_workspace_path, key, tokensets, num_topics):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self.daemon = True
        self._notify_window = notify_window
        self.current_workspace_path = current_workspace_path
        self.key = key
        self.tokensets = tokensets
        self.num_topics = num_topics
        self.start()
        os.environ["TOKENIZERS_PARALLELISM"] = "false"

    # def run(self):
    #     '''Generates an ETM model'''
    #     logger = logging.getLogger(__name__ + "ETMTrainingThread[" + str(self.key) + "].run")
    #     logger.info("Starting")
    #     t1 = time.time()
    #
    #     if not os.path.exists(self.current_workspace_path + "/Samples/" + self.key):
    #         os.makedirs(self.current_workspace_path + "/Samples/" + self.key)
    #
    #     text_keys = []
    #     texts = []
    #     for key in self.tokensets:
    #         text_keys.append(key)
    #         text = ' '.join(self.tokensets[key])
    #         texts.append(text)
    #
    #     logger.info("Starting generation of ETM model")
    #     vectorizer = CountVectorizer(stop_words='english')  # Add max_features?
    #     X = vectorizer.fit_transform(texts)
    #     vocab = vectorizer.get_feature_names_out()
    #     tokens_list = [list(map(int, doc.nonzero()[1])) for doc in X]
    #     counts_list = [list(map(int, doc.data)) for doc in X]
    #     tokens_tensor = [torch.tensor(tokens, dtype=torch.int64) for tokens in tokens_list]
    #     counts_tensor = [torch.tensor(counts, dtype=torch.float32) for counts in counts_list]
    #
    #     train_data = {
    #         'tokens': tokens_tensor,
    #         'counts': counts_tensor,
    #     }
    #     vocab_size = len(vocab)
    #     embedding_size = 300
    #     model = ETM(vocab, num_topics=self.num_topics+1, epochs=100, t_hidden_size=128, rho_size=embedding_size, theta_act='relu')
    #     model.fit(train_data)
    #
    #     topics = model.get_topics()
    #     print(topics)
    #
    #     topic_word_dist = model.get_topic_word_dist()
    #
    #     updated_topic_info = {}
    #     for topic_idx, word_dist in enumerate(topic_word_dist):
    #         sorted_indices = np.argsort(-word_dist)  # Sort in descending order of probability
    #         topic_words = [(vocab[i], word_dist[i]) for i in sorted_indices[:10]]  # Get top 10 words per topic
    #         updated_topic_info[topic_idx] = topic_words
    #
    #     print("Updated topic information:")
    #     for topic_id, topic_words in updated_topic_info.items():
    #         print(f"Topic {topic_id}:")
    #         for word, prob in topic_words:
    #             print(f"- {word} (probability: {prob:.4f})")
    #
    #
    #     document_topic_prob = model.transform(train_data).numpy()
    #     print(document_topic_prob)
    #     # print(topics)
    #
    #
    #     start_time = time.time()
    #     file_path = "model_topics_list.txt"
    #
    #     with open(file_path, "w") as file:
    #         # Instantiate the TopicDiversity metric
    #         metric = TopicDiversity(topk=10)
    #         model_output = {"topics": []}
    #
    #         topic = 0
    #         for topic_info in updated_topic_info:
    #             # Get the topic information using topic_model.get_topic(topic)
    #
    #             # If the topic information is empty, break out of the loop
    #             if not topic_info:
    #                 break
    #
    #             # Append the topic information to the list under the "topics" key
    #             model_output["topics"].append(topic_info)
    #
    #             # Write the topic number
    #             file.write(f"Topic {topic}:\n")
    #
    #             # Write the keywords and their probabilities
    #             for keyword, probability in topic_info:
    #                 file.write(f"('{keyword}', {probability})\n")
    #
    #             # Write a newline to separate topics
    #             file.write("\n")
    #
    #             # Increment the topic index
    #             topic += 1
    #
    #     end_time = time.time()
    #     time_taken = end_time - start_time
    #     print("================================================")
    #     print("Topic Coherence for ETM: {0}".format(model.get_topic_coherence))
    #     print("Topic Diversity for ETM: {0}".format(model.get_topic_diversity()))
    #     # Print and log the time taken
    #     print(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
    #     logger.info(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
    #     t2 = time.time()
    #     tots = t2 - t1
    #     print(f"Time taken Total: {tots:.4f} seconds")
    #     print("================================================")
    #
    #     logger.info("Starting Generation of ETM")
    #
    #     with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/tfidf_vectorizer.pk',
    #                      'wb') as outfile:
    #         pickle.dump(vectorizer, outfile)
    #
    #     with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/ETM_model.pk', 'wb') as outfile:
    #         pickle.dump(model, outfile)
    #     positive_topic_indices = [i for i in range(document_topic_prob.shape[1]) if i > 0]
    #     filtered_topic_prob_df = pd.DataFrame(document_topic_prob[:, positive_topic_indices],
    #                                           columns=[f"Topic {i}" for i in positive_topic_indices])
    #     result = {'key': self.key, 'document_topic_prob': {}}
    #
    #     for i, row in enumerate(filtered_topic_prob_df.values):
    #         formatted_key = text_keys[i]  # Use the document key (text key)
    #         result['document_topic_prob'][formatted_key] = {j + 1: prob for j, prob in enumerate(row)}
    #     # print(result)
    #     logger.info("Completed Generation of ETM")
    #     wx.PostEvent(self._notify_window, CustomEvents.ModelCreatedResultEvent(result))
    #
    #
    #
    #
    #
    #
    #

    def run(self):
        '''Generates an ETM model'''
        logger = logging.getLogger(__name__ + "ETMTrainingThread[" + str(self.key) + "].run")
        logger.info("Starting")
        t1 = time.time()

        if not os.path.exists(self.current_workspace_path + "/Samples/" + self.key):
            os.makedirs(self.current_workspace_path + "/Samples/" + self.key)

        text_keys = []
        texts = []
        for key in self.tokensets:
            text_keys.append(key)
            text = ' '.join(self.tokensets[key])
            texts.append(text)

        data = [(key, ' '.join(tokens)) for key, tokens in self.tokensets.items()]

        # Extract keys and texts separately
        text_keys, texts = zip(*data)

        logger.info("Starting generation of ETM model")
        vectorizer = CountVectorizer(stop_words='english')  # Add max_features?
        X = vectorizer.fit_transform(texts)
        # print(self.tokensets)
        # print(texts)
        vocab = vectorizer.get_feature_names_out()
        # print(vocab)
        tokenized_texts = list(self.tokensets.values())
        # print(tokenized_texts)

        documents = [{'text': text} for text in texts]

        train_texts, test_texts = train_test_split(tokenized_texts, test_size=0.2, random_state=42)
        train_texts, val_texts = train_test_split(train_texts, test_size=0.25, random_state=42)

        dataset = Dataset()
        dataset._dataset = {
            "texts": texts,
            "metadata": {
                'vocabulary_length': len(vocab),
                'last-training-doc': len(train_texts) - 1,
                # 'last-validation-doc': len(train_texts) + len(val_texts) - 1,
                'use-validation': False,
            },
            "vocabulary": vocab,
            "corpus": tokenized_texts,
        }

        dataset._Dataset__metadata = {}
        dataset._Dataset__vocabulary = vocab
        dataset._Dataset__metadata = {
            'vocabulary_length': len(vocab),
            'last-training-doc': len(train_texts) - 1,
            # 'last-validation-doc': len(train_texts) + len(val_texts) - 1,
            'use-validation': False,
        }
        dataset._Dataset__corpus = tokenized_texts
        # vocabulary = list(set(" ".join(texts).split()))
        # dataset.vocabulary = vocabulary
        # partitioned_corpus = self.custom_train_val_test_split(documents)
        # dataset._Dataset__partitioned_corpus = partitioned_corpus

        # dataset = Dataset()
        # dataset.fetch_dataset("20NewsGroup")

        model = MyETM(num_topics=self.num_topics+1, num_epochs=20)
        print("Pipeline created successfully")
        print("ETM model initialized successfully")
        output = model.train_model(dataset)
        topics = output['topics']
        topic_document_matrix = output['topic-document-matrix']

        number_of_topics = 0

        print("Updated topic information:")
        for topic_id, topic_words in enumerate(topics):
            number_of_topics += 1
            print(f"Topic {topic_id}:")
            for index, word in enumerate(topic_words):
                print(f"- {word} (probability: {topic_document_matrix[topic_id][index]:.4f})")

        print(f"Number of topics {number_of_topics}")

        cv = Coherence(texts=dataset.get_corpus(), topk=10, measure='c_v')
        start_time = time.time()
        print("================================================")
        # Print coherence score
        print(f'Topic Coherence for ETM: {cv.score(output)}')

        file_path = "model_topics_list.txt"

        with open(file_path, "w") as file:
            # Instantiate the TopicDiversity metric
            metric = TopicDiversity(topk=10)
            model_output = {"topics": []}

            for topic, topic_info in enumerate(topics):

                # Append the topic information to the list under the "topics" key
                model_output["topics"].append(topic_info)

                # Write the topic number
                file.write(f"Topic {topic}:\n")

                # Write the keywords and their probabilities
                for index, keyword in enumerate(topic_info):
                    file.write(f"('{keyword}', {topic_document_matrix[topic][index]})\n")

                # Write a newline to separate topics
                file.write("\n")

                # Increment the topic index
                topic += 1

        topic_diversity_metric = TopicDiversity(topk=10)
        topic_diversity_score = topic_diversity_metric.score(output)
        print("Topic Diversity Score is: {0}".format(topic_diversity_score))
        # kl_divergence_metric = Coherence(topk=10, measure='kl')
        # kl_divergence_score = kl_divergence_metric.score(output)
        # print(f"Average KL Divergence for ETM: {kl_divergence_score:.4f}")
        # logger.info(f"Average KL Divergence for ETM: {kl_divergence_score:.4f}")

        num_topics = topic_document_matrix.shape[1]
        uniform_distribution = np.ones(num_topics) / num_topics

        # Compute the KL divergence
        kl_divergences = [np.sum(rel_entr(doc_dist, uniform_distribution)) for doc_dist in topic_document_matrix]

        # Print the average KL divergence
        average_kl_divergence = np.mean(kl_divergences)
        print(f"Average KL Divergence for ETM: {average_kl_divergence:.4f}")
        logger.info(f"Average KL Divergence for ETM: {average_kl_divergence:.4f}")

        log_perplexity = -1 * np.mean(np.log(np.sum(topic_document_matrix, axis=0)))
        perplexity = np.exp(log_perplexity)

        # Print and log the perplexity
        print(f"Perplexity for ETM: {perplexity:.4f}")
        logger.info(f"Perplexity for ETM: {perplexity:.4f}")

        # End measuring the time taken from coherence to perplexity
        end_time = time.time()
        time_taken = end_time - start_time

        # Print and log the time taken
        print(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        logger.info(f"Time taken from coherence to perplexity calculation: {time_taken:.4f} seconds")
        t2 = time.time()
        tots = t2 - t1
        print(f"Time taken Total: {tots:.4f} seconds")
        print("================================================")

        logger.info("Completed ETM model and calculated KL divergence and perplexity")
        logger.info("Finished")

        logger.info("Texts transformed")

        logger.info("Starting Generation of ETM")

        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/tfidf_vectorizer.pk',
                         'wb') as outfile:
            pickle.dump(vectorizer, outfile)

        with bz2.BZ2File(self.current_workspace_path + "/Samples/" + self.key + '/ETM_model.pk', 'wb') as outfile:
            pickle.dump(output, outfile)

        logger.info("Completed Generation of ETM")
        # Fit the BERTopic model on the text data

        # Filter columns to include only topics with positive indices
        topic_document_matrix = topic_document_matrix.transpose()
        test_topic_document_matrix = output["test-topic-document-matrix"].transpose()

        topic_document_matrix = np.concatenate([topic_document_matrix, test_topic_document_matrix])
        positive_topic_indices = [i for i in range(topic_document_matrix.shape[1]) if i > 0]
        filtered_topic_prob_df = pd.DataFrame(topic_document_matrix[:, positive_topic_indices],
                                              columns=[f"Topic {i}" for i in positive_topic_indices])

        # Print the filtered DataFrame
        # print(filtered_topic_prob_df)

        # Prepare the result dictionary
        result = {'key': self.key, 'document_topic_prob': {}}

        for i, row in enumerate(filtered_topic_prob_df.values):
            formatted_key = text_keys[i]  # Use the document key (text key)
            result['document_topic_prob'][formatted_key] = {j + 1: prob for j, prob in enumerate(row)}

        # # Convert the NumPy array to a DataFrame
        # document_topic_prob_df = pd.DataFrame(document_topic_prob, columns=[f"Topic {i}" for i in range(document_topic_prob.shape[1])])
        # print(document_topic_prob_df)

        # result = {'key': self.key, 'document_topic_prob': {}}

        # for i, row in enumerate(document_topic_prob_df.values):
        #     formatted_key = text_keys[i]  # Remove the extra ()
        #     result['document_topic_prob'][formatted_key] = {j + 1: prob for j, prob in enumerate(row)}

        # print(result)
        wx.PostEvent(self._notify_window, CustomEvents.ModelCreatedResultEvent(result))
